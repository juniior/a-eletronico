﻿using NITGEN.SDK.NBioBSP;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace WinFormCharpWebCam
{
    public partial class SIREP : Form
    {
        #region Atributos

        NBioAPI m_NBioAPI;
        NBioAPI.IndexSearch m_IndexSearch;

        short m_OpenedDeviceID;
        NBioAPI.Type.HFIR m_hNewFIR;

        NBioAPI.Type.FIR m_biFIR;
        NBioAPI.Type.FIR_TEXTENCODE m_textFIR;

        WebCam webcam;

        der_db01Entities bd = new der_db01Entities();

        #endregion

        public SIREP()
        {
            InitializeComponent();
            try
            {
                m_NBioAPI = new NBioAPI();
                m_IndexSearch = new NBioAPI.IndexSearch(m_NBioAPI);

                m_OpenedDeviceID = NBioAPI.Type.DEVICE_ID.NONE;
                m_hNewFIR = null;
            }
            catch (Exception ex)
            {
                lblErro.Text = ex.Message;
                lblErro.ForeColor = System.Drawing.Color.Red;
            }
        }

        #region Métodos Eventos

        private void btnLocalizarServidor_Click(object sender, EventArgs e)
        {
            try
            {
                der_db01Entities bd = new der_db01Entities();

                Usuario usuario = bd.Usuario.SingleOrDefault(c => c.MATRICULA == txtMatriculaServidor.Text && c.ATIVO == true);

                txtNomeServidor.Text = usuario.NOME;
                txtCPFServidor.Text = usuario.CPF_FORMATADO;

                CarregarFoto(usuario);

                Mensagens("\nServidor encontrado: " + usuario.MATRICULA);
            }
            catch (Exception ex)
            {
                lblErro.Text = ex.Message;
                lblErro.ForeColor = System.Drawing.Color.Red;
            }
        }

        private void btnEnroll_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtMatriculaServidor.Text == string.Empty)
                    throw new Exception("Matricula em branco!");

                m_hNewFIR = null;

                NBioAPI.Type.HFIR NewFIR;
                NBioAPI.Type.FIR_PAYLOAD myPayload = new NBioAPI.Type.FIR_PAYLOAD();


                Usuario usuario = bd.Usuario.SingleOrDefault(c => c.MATRICULA == txtMatriculaServidor.Text && c.ATIVO == true);

                if (usuario == null)
                    throw new Exception("Servidor não encontrado! Verifique se a matricula existe ou se foi cadastrado!");

                myPayload.Data = usuario.USUARIO_ID.ToString();

                uint ret = m_NBioAPI.Enroll(ref m_hNewFIR, out NewFIR, myPayload, NBioAPI.Type.TIMEOUT.DEFAULT, null, null);

                if (ret == NBioAPI.Error.NONE)
                {
                    if (m_hNewFIR != null)
                    {
                        // Free native memory
                        //m_hNewFIR.Dispose();
                    }

                    m_hNewFIR = NewFIR;

                    // If you want to save fir data then this FIR data write to DB or File.
                    // Se você deseja salvar os dados abeto então esses dados FIR escrever para DB ou Arquivo.

                    m_NBioAPI.GetFIRFromHandle(m_hNewFIR, out m_biFIR);// Get binary encoded FIR data

                    // Get text encoded FIR data

                    //NBioAPI.Type.FIR_TEXTENCODE m_textFIR;
                    m_NBioAPI.GetTextFIRFromHandle(m_hNewFIR, out m_textFIR, true);

                    Bio bio = new Bio();

                    string mensagem = "";

                    // Se não existir as digitais cadastradas: Cadastra
                    if (!ExisteFIR(usuario))
                    {
                        bio.USUARIO_ID = usuario.USUARIO_ID;
                        bio.FIR = m_textFIR.TextFIR.ToString();

                        bd.Bio.Add(bio);

                        mensagem = "Digitais cadastradas com sucesso!";
                        Mensagens("\nDigital registrada: " + usuario.MATRICULA);
                    }
                    else
                    {
                        bio = bd.Bio.SingleOrDefault(c => c.USUARIO_ID == usuario.USUARIO_ID);
                        bio.FIR = m_textFIR.TextFIR.ToString();

                        mensagem = "Digitais Atualizadas com sucesso!";
                        Mensagens("\nDigital atualizada: " + usuario.MATRICULA);
                    }
                    bd.SaveChanges();

                    labStatus.Text = mensagem;
                }
                else
                {
                    DisplayErrorMsg("Enroll Function Failed! - ", ret);
                }
            }
            catch (Exception ex)
            {
                lblErro.Text = ex.Message;
                lblErro.ForeColor = Color.Red;

            }
        }// Fim do Método

        private void SIREP_Load(object sender, EventArgs e)
        {
            try
            {
                DireitosAutorais();
                webcam = new WebCam();
                webcam.InitializeWebCam(ref imgVideo);
                short iDeviceID = NBioAPI.Type.DEVICE_ID.AUTO;

                iDeviceID = NBioAPI.Type.DEVICE_ID.AUTO;

                // Close Device if before opened
                m_NBioAPI.CloseDevice(m_OpenedDeviceID);

                // Open device
                uint ret = m_NBioAPI.OpenDevice(iDeviceID);
                if (ret == NBioAPI.Error.NONE)
                {
                    m_OpenedDeviceID = iDeviceID;

                    btnEnroll.Enabled = true;
                    labStatus.Text = "Leitor Ok! - ";

                    labStatus.Text += "Auto Detect Device";
                }
                else
                {
                    DisplayErrorMsg("Falha no leitor! - ", ret);
                }
                lblMensagens.Text = string.Empty;
                Mensagens("Sistema carregado!");
            }
            catch (Exception ex)
            {
                lblErro.Text = ex.Message;
                lblErro.ForeColor = System.Drawing.Color.Red;
            }
        }// Fim do Método 

        private void SIREP_Closed(object sender, System.EventArgs e)
        {
            // Close Device
            m_NBioAPI.CloseDevice(m_OpenedDeviceID);
        }// Fim do Método 

        private void bntStart_Click(object sender, EventArgs e)
        {
            try
            {
                webcam.Start();
            }
            catch (Exception ex)
            {
                lblErro.Text = ex.Message;
                lblErro.ForeColor = System.Drawing.Color.Red;
            }
        }

        private void bntCapture_Click(object sender, EventArgs e)
        {
            try
            {
                imgCapture.Image = imgVideo.Image;
                SalvarFoto();
                Mensagens("\nFoto inserida: " + txtMatriculaServidor.Text);
            }
            catch (Exception ex)
            {
                lblErro.Text = ex.Message;
                lblErro.ForeColor = System.Drawing.Color.Red;
            }
        }

        private void txtMatriculaServidor_TextChanged(object sender, EventArgs e)
        {
            btnLocalizarServidor.Enabled = txtMatriculaServidor.Text.Trim() != "";
        }

        private void btnVerificarDigital_Click(object sender, EventArgs e)
        {
            try
            {
                m_NBioAPI.OpenDevice(NBioAPI.Type.DEVICE_ID.AUTO);
                NBioAPI.Type.HFIR capturedFIR;
                uint ret = m_NBioAPI.Capture(NBioAPI.Type.FIR_PURPOSE.VERIFY, out capturedFIR, NBioAPI.Type.TIMEOUT.DEFAULT, null, null);

                if (ret != NBioAPI.Error.NONE)
                {
                    MessageBox.Show("Falha ao capturar digital");
                    return;
                }

                der_db01Entities bd = new der_db01Entities();

                m_NBioAPI.GetTextFIRFromHandle(capturedFIR, out m_textFIR, true);

                var _bios = bd.Bio.ToList();

                bool result = false;
                NBioAPI.Type.FIR_PAYLOAD myPayload = new NBioAPI.Type.FIR_PAYLOAD();

                foreach (var item in _bios)
                {
                    m_textFIR.TextFIR = item.FIR;
                    m_NBioAPI.VerifyMatch(capturedFIR, m_textFIR, out result, myPayload);
                    if (result)
                    {
                        Usuario usuario = bd.Usuario.SingleOrDefault(c => c.USUARIO_ID == item.USUARIO_ID);
                        txtMatriculaServidor.Text = usuario.MATRICULA;
                        txtCPFServidor.Text = usuario.CPF_FORMATADO.ToString();
                        break;
                    }
                }

                if (result)
                {
                    btnLocalizarServidor_Click(null, e);
                }
                else MessageBox.Show("Servidor não encontrado");

                m_NBioAPI.CloseDevice(NBioAPI.Type.DEVICE_ID.AUTO);
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            imgVideo.Image = imgCapture.Image = null;
            txtCPFServidor.Text = txtMatriculaServidor.Text = txtNomeServidor.Text = lblErro.Text = string.Empty;
            webcam.Stop();
        }

        #endregion

        #region Métodos Privados

        private void Mensagens(string Mensagem)
        {
            lblMensagens.Text += Mensagem;
        }

        private void DireitosAutorais()
        {
            lblDireitos.Text = "Sistema desenvolvido pela equipe de TI DER/RO. \nPara Suporte Ligue: (69) 3216-7372 \nProgramador responsavel: Osvaldo dos Santos Junior.";
            lblInstrucoes.Text = "Instruções:\n1- Certifique-se que o servidor tenha Matricula;\n2- Certifique-se que o servidor ja fez o cadastro no sistema on-line;";
            lblInstrucoes.Text += "\n3- Digite a matricula do servidor e mande localizar, será carregado os dados do servidor!";
            lblInstrucoes.Text += "\n4- Clique no botão \"Digitais\" abrirá uma janela, siga os passos;\n5- Caso tenha uma webcam na maquina, clique no botão \"Abre WebCam\" para abrir;\n6- Depois em \"Capture Image\" para salvar a foto no perfil do servidor!\n7- Para um novo registro, clique no botão \"Novo\";";
        }

        private Image ByteArrayToImage(byte[] byteArrayIn)
        {
            using (MemoryStream ms = new MemoryStream(byteArrayIn))
            {
                Image returnImage = Image.FromStream(ms);
                return returnImage;
            }
        }

        private void CarregarFoto(Usuario usuario)
        {
            if (usuario.FOTO == null)
            {
                imgCapture.Image = null;
            }
            else
            {
                imgCapture.Image = ByteArrayToImage(usuario.FOTO.ToArray());
            }
        }

        private void DisplayErrorMsg(string msg, uint ret)
        {
            labStatus.Text = msg + NBioAPI.Error.GetErrorDescription(ret) + " [" + ret.ToString() + "]";
        }

        private bool ExisteFIR(Usuario usuario)
        {
            try
            {
                der_db01Entities bd = new der_db01Entities();
                bool existe = false;
                Bio bio = bd.Bio.SingleOrDefault(c => c.USUARIO_ID == usuario.USUARIO_ID);

                if (bio != null)
                    existe = true;

                return existe;
            }
            catch (Exception)
            {
                throw;
            }
        }// Fim do Método 

        private void SalvarFoto()
        {
            der_db01Entities bd = new der_db01Entities();
            byte[] file_byte = ImageToByteArray(imgCapture.Image);
            System.Data.Linq.Binary file_binary = new System.Data.Linq.Binary(file_byte);

            Usuario usuario = bd.Usuario.SingleOrDefault(c => c.MATRICULA == txtMatriculaServidor.Text && c.ATIVO == true);

            usuario.FOTO = file_binary.ToArray();

            bd.SaveChanges();

            MessageBox.Show("Foto tirada com sucesso!", "Mensagem: by Juniior");


        }

        private byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
                return ms.ToArray();
            }
        }

        #endregion

    }
}
