﻿namespace WinFormCharpWebCam
{
    partial class SIREP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SIREP));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.imgVideo = new System.Windows.Forms.PictureBox();
            this.bntCapture = new System.Windows.Forms.Button();
            this.bntStart = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMatriculaServidor = new System.Windows.Forms.TextBox();
            this.btnLocalizarServidor = new System.Windows.Forms.Button();
            this.btnVerificarDigital = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.imgCapture = new System.Windows.Forms.PictureBox();
            this.txtNomeServidor = new System.Windows.Forms.TextBox();
            this.txtCPFServidor = new System.Windows.Forms.TextBox();
            this.btnEnroll = new System.Windows.Forms.Button();
            this.labStatus = new System.Windows.Forms.StatusBarPanel();
            this.StatusBar = new System.Windows.Forms.StatusBar();
            this.lblErro = new System.Windows.Forms.Label();
            this.gbMensagens = new System.Windows.Forms.GroupBox();
            this.lblDireitos = new System.Windows.Forms.Label();
            this.lblMensagens = new System.Windows.Forms.Label();
            this.gbLogo = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lblInstrucoes = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgVideo)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCapture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labStatus)).BeginInit();
            this.gbMensagens.SuspendLayout();
            this.gbLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.imgVideo);
            this.groupBox2.Controls.Add(this.bntCapture);
            this.groupBox2.Controls.Add(this.bntStart);
            this.groupBox2.Location = new System.Drawing.Point(8, 268);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(506, 151);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "WebCam";
            // 
            // imgVideo
            // 
            this.imgVideo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgVideo.Location = new System.Drawing.Point(9, 19);
            this.imgVideo.Name = "imgVideo";
            this.imgVideo.Size = new System.Drawing.Size(160, 120);
            this.imgVideo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgVideo.TabIndex = 16;
            this.imgVideo.TabStop = false;
            // 
            // bntCapture
            // 
            this.bntCapture.Location = new System.Drawing.Point(179, 75);
            this.bntCapture.Name = "bntCapture";
            this.bntCapture.Size = new System.Drawing.Size(160, 24);
            this.bntCapture.TabIndex = 20;
            this.bntCapture.Text = "Capture Image";
            this.bntCapture.UseVisualStyleBackColor = true;
            this.bntCapture.Click += new System.EventHandler(this.bntCapture_Click);
            // 
            // bntStart
            // 
            this.bntStart.Location = new System.Drawing.Point(179, 19);
            this.bntStart.Name = "bntStart";
            this.bntStart.Size = new System.Drawing.Size(160, 23);
            this.bntStart.TabIndex = 17;
            this.bntStart.Text = "Abre WebCam";
            this.bntStart.UseVisualStyleBackColor = true;
            this.bntStart.Click += new System.EventHandler(this.bntStart_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtMatriculaServidor);
            this.groupBox1.Controls.Add(this.btnLocalizarServidor);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(506, 74);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Localizar Servidor";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Matricula";
            // 
            // txtMatriculaServidor
            // 
            this.txtMatriculaServidor.Location = new System.Drawing.Point(54, 31);
            this.txtMatriculaServidor.Name = "txtMatriculaServidor";
            this.txtMatriculaServidor.Size = new System.Drawing.Size(138, 20);
            this.txtMatriculaServidor.TabIndex = 13;
            this.txtMatriculaServidor.TextChanged += new System.EventHandler(this.txtMatriculaServidor_TextChanged);
            // 
            // btnLocalizarServidor
            // 
            this.btnLocalizarServidor.Enabled = false;
            this.btnLocalizarServidor.Location = new System.Drawing.Point(199, 29);
            this.btnLocalizarServidor.Name = "btnLocalizarServidor";
            this.btnLocalizarServidor.Size = new System.Drawing.Size(106, 23);
            this.btnLocalizarServidor.TabIndex = 12;
            this.btnLocalizarServidor.Text = "Localizar Servidor";
            this.btnLocalizarServidor.UseVisualStyleBackColor = true;
            this.btnLocalizarServidor.Click += new System.EventHandler(this.btnLocalizarServidor_Click);
            // 
            // btnVerificarDigital
            // 
            this.btnVerificarDigital.Location = new System.Drawing.Point(150, 21);
            this.btnVerificarDigital.Name = "btnVerificarDigital";
            this.btnVerificarDigital.Size = new System.Drawing.Size(155, 24);
            this.btnVerificarDigital.TabIndex = 16;
            this.btnVerificarDigital.Text = "&Verificar digital";
            this.btnVerificarDigital.Click += new System.EventHandler(this.btnVerificarDigital_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.Location = new System.Drawing.Point(415, 19);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpar.TabIndex = 17;
            this.btnLimpar.Text = "Novo";
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnLimpar);
            this.groupBox4.Controls.Add(this.btnVerificarDigital);
            this.groupBox4.Location = new System.Drawing.Point(8, 425);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(506, 51);
            this.groupBox4.TabIndex = 25;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Verificar Digitais";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.imgCapture);
            this.groupBox3.Controls.Add(this.txtNomeServidor);
            this.groupBox3.Controls.Add(this.txtCPFServidor);
            this.groupBox3.Controls.Add(this.btnEnroll);
            this.groupBox3.Location = new System.Drawing.Point(8, 86);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(506, 176);
            this.groupBox3.TabIndex = 24;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Servidor";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(178, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "CPF";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(178, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Nome";
            // 
            // imgCapture
            // 
            this.imgCapture.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgCapture.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imgCapture.Location = new System.Drawing.Point(9, 19);
            this.imgCapture.Name = "imgCapture";
            this.imgCapture.Size = new System.Drawing.Size(160, 120);
            this.imgCapture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgCapture.TabIndex = 8;
            this.imgCapture.TabStop = false;
            // 
            // txtNomeServidor
            // 
            this.txtNomeServidor.Location = new System.Drawing.Point(214, 19);
            this.txtNomeServidor.Name = "txtNomeServidor";
            this.txtNomeServidor.Size = new System.Drawing.Size(276, 20);
            this.txtNomeServidor.TabIndex = 5;
            // 
            // txtCPFServidor
            // 
            this.txtCPFServidor.Location = new System.Drawing.Point(214, 55);
            this.txtCPFServidor.Name = "txtCPFServidor";
            this.txtCPFServidor.Size = new System.Drawing.Size(120, 20);
            this.txtCPFServidor.TabIndex = 0;
            // 
            // btnEnroll
            // 
            this.btnEnroll.Enabled = false;
            this.btnEnroll.Location = new System.Drawing.Point(9, 145);
            this.btnEnroll.Name = "btnEnroll";
            this.btnEnroll.Size = new System.Drawing.Size(160, 24);
            this.btnEnroll.TabIndex = 1;
            this.btnEnroll.Text = "&Digitais";
            this.btnEnroll.Click += new System.EventHandler(this.btnEnroll_Click);
            // 
            // labStatus
            // 
            this.labStatus.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            this.labStatus.Name = "labStatus";
            this.labStatus.Width = 858;
            // 
            // StatusBar
            // 
            this.StatusBar.Location = new System.Drawing.Point(0, 720);
            this.StatusBar.Name = "StatusBar";
            this.StatusBar.Panels.AddRange(new System.Windows.Forms.StatusBarPanel[] {
            this.labStatus});
            this.StatusBar.ShowPanels = true;
            this.StatusBar.Size = new System.Drawing.Size(874, 24);
            this.StatusBar.TabIndex = 22;
            // 
            // lblErro
            // 
            this.lblErro.AutoSize = true;
            this.lblErro.BackColor = System.Drawing.Color.BurlyWood;
            this.lblErro.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErro.Location = new System.Drawing.Point(12, 651);
            this.lblErro.Name = "lblErro";
            this.lblErro.Size = new System.Drawing.Size(18, 26);
            this.lblErro.TabIndex = 27;
            this.lblErro.Text = ".";
            // 
            // gbMensagens
            // 
            this.gbMensagens.Controls.Add(this.lblDireitos);
            this.gbMensagens.Controls.Add(this.lblMensagens);
            this.gbMensagens.Location = new System.Drawing.Point(521, 163);
            this.gbMensagens.Name = "gbMensagens";
            this.gbMensagens.Size = new System.Drawing.Size(341, 456);
            this.gbMensagens.TabIndex = 28;
            this.gbMensagens.TabStop = false;
            this.gbMensagens.Text = "Mensagens";
            // 
            // lblDireitos
            // 
            this.lblDireitos.AutoSize = true;
            this.lblDireitos.Location = new System.Drawing.Point(7, 20);
            this.lblDireitos.Name = "lblDireitos";
            this.lblDireitos.Size = new System.Drawing.Size(49, 13);
            this.lblDireitos.TabIndex = 1;
            this.lblDireitos.Text = "Corprigth";
            // 
            // lblMensagens
            // 
            this.lblMensagens.AutoSize = true;
            this.lblMensagens.Location = new System.Drawing.Point(7, 80);
            this.lblMensagens.Name = "lblMensagens";
            this.lblMensagens.Size = new System.Drawing.Size(62, 13);
            this.lblMensagens.TabIndex = 0;
            this.lblMensagens.Text = "Mensagens";
            // 
            // gbLogo
            // 
            this.gbLogo.Controls.Add(this.pictureBox1);
            this.gbLogo.Location = new System.Drawing.Point(520, 6);
            this.gbLogo.Name = "gbLogo";
            this.gbLogo.Size = new System.Drawing.Size(341, 150);
            this.gbLogo.TabIndex = 29;
            this.gbLogo.TabStop = false;
            this.gbLogo.Text = "DER/RO";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::WinFormCharpWebCam.Properties.Resources.logo_der_si;
            this.pictureBox1.Location = new System.Drawing.Point(25, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(287, 105);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lblInstrucoes);
            this.groupBox5.Location = new System.Drawing.Point(8, 483);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(506, 136);
            this.groupBox5.TabIndex = 30;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Instruções";
            // 
            // lblInstrucoes
            // 
            this.lblInstrucoes.AutoSize = true;
            this.lblInstrucoes.Location = new System.Drawing.Point(7, 20);
            this.lblInstrucoes.Name = "lblInstrucoes";
            this.lblInstrucoes.Size = new System.Drawing.Size(62, 13);
            this.lblInstrucoes.TabIndex = 0;
            this.lblInstrucoes.Text = "Instrucoes: ";
            // 
            // SIREP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 744);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.gbLogo);
            this.Controls.Add(this.gbMensagens);
            this.Controls.Add(this.lblErro);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.StatusBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SIREP";
            this.Text = "SIREP - Cadastro de Digitais e Foto Perfil";
            this.Load += new System.EventHandler(this.SIREP_Load);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgVideo)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCapture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labStatus)).EndInit();
            this.gbMensagens.ResumeLayout(false);
            this.gbMensagens.PerformLayout();
            this.gbLogo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox imgVideo;
        private System.Windows.Forms.Button bntCapture;
        private System.Windows.Forms.Button bntStart;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtMatriculaServidor;
        private System.Windows.Forms.Button btnLocalizarServidor;
        private System.Windows.Forms.Button btnVerificarDigital;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox imgCapture;
        private System.Windows.Forms.TextBox txtNomeServidor;
        private System.Windows.Forms.TextBox txtCPFServidor;
        private System.Windows.Forms.Button btnEnroll;
        private System.Windows.Forms.StatusBarPanel labStatus;
        private System.Windows.Forms.StatusBar StatusBar;
        private System.Windows.Forms.Label lblErro;
        private System.Windows.Forms.GroupBox gbMensagens;
        private System.Windows.Forms.Label lblMensagens;
        private System.Windows.Forms.Label lblDireitos;
        private System.Windows.Forms.GroupBox gbLogo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label lblInstrucoes;
    }
}