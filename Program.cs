﻿using System;
using System.Windows.Forms;
using WinFormCharpWebCam;

namespace BSPDemoCS
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            //Application.Run(new FormDigitais());
            Application.Run(new SIREP());
        }

    }
}
